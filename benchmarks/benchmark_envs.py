import timeit
from functools import partial

from gym_vail.envs import Bandit, Maze, MemoryGame, TabularMDP


def benchmark_env(env_f, episodes):
    env = env_f()
    for _ in range(episodes):
        env.reset()
        done = False
        while not done:
            action = env.action_space.sample()
            obs, reward, done, info = env.step(action)


retries = 10
episode_count = 100
for env_f in [Bandit, Maze, MemoryGame, TabularMDP]:
    timer = timeit.Timer(partial(benchmark_env, env_f, episode_count))
    duration = timer.timeit(retries)
    print(
        f"{env_f.__name__}:"
        f"\n\tEpisodes / Trial: {episode_count}"
        f"\n\tTrials:           {retries}"
        f"\n\tTotal Duration:   {duration:0.4f}"
        f"\n\tAverage Duration: {duration / retries:0.4f}\n"
    )
