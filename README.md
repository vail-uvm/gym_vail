# gym_vail 
Environments implemented for VaiL projects that conform to the OpenAI Gym interface.

## Available Environments

- `Bandit-v0`:
    A dynamic multi-armed bandit problem, as seen in [RL^2](https://arxiv.org/abs/1611.02779).
    Default settings use 50 arms for 100 trials with rewards determined by Bernoulli distributions with parameter drawn
    from a uniform [0, 1] random variable at the start of each episode.
    According to [RL^2](https://arxiv.org/abs/1611.02779), optimal performance results in scores of ~85.
- `Doom-v0`:
    A partially observable 3D environment with visual features, developed using [VizDoom](https://github.com/mwydmuch/ViZDoom).
- `Maze-v0`:
    A configurable maze environment with a hidden reward tile.
    Agents must explore the maze to locate the tile, then exploit the reward tile once it has been found.
    The reward tile is not visible to the agent, thus the location must be inferred indirectly and memorized.
    Based on the environments described in 
    ["Differentiable plasticity: training plastic neural networks with backpropagation"](https://arxiv.org/abs/1804.02464)
    and their [reference implementation](https://github.com/uber-research/differentiable-plasticity/blob/master/maze/maze.py).
- `Memory-v0`:
    An implementation of the card game [Memory](https://en.wikipedia.org/wiki/Concentration_(card_game), which is also called Concentration, Match Up, and more.
    Agents must reveal pairs cards sequentially in an effort to reveal the entire board.
    
## Getting Started
This repository is pip installable:
```bash
git clone https://gitlab.com/vail-uvm/gym_vail.git
cd gym_vail
pip install -e .
```

Environments implemented in `gym_vail` are automatically registered with `gym` on import:
```python
import gym
import gym_vail

# Create a gym_vail env using OpenAI gym tools
env = gym.envs.make('Maze-v0')
```

## Adding New Environments

- Create a new branch, suggested naming convention is `feature/{your env name}` (bonus points for using [Git-Flow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)).
- Implement your environment as one or more files in the `gym_vail/envs` folder.
  - Each env must implement the `step`, `reset`, and `render` functions, see `gym_vail/envs/maze.py` for an example.
  - The [OpenAI gym documentation](https://github.com/openai/gym/blob/master/docs/creating-environments.md) also has some additional information on creating new environments.
  - If your environment is complicated and involves several source files of implementation,
  then you may wish to enclose it within a subdirectory
- Import your env in `gym_vail/envs/__init_.py`, exposing it as part of the package.
- Register your env in `gym_vail/__init_.py`, allowing it to function with OpenAI `gym` utilities simply by importing `gym_vail`.