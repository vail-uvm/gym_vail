from setuptools import find_packages, setup

with open("requirements.txt") as f:
    requirements = [line.strip() for line in f]

setup(
    name="gym_vail",
    version="1.1.0",
    install_requires=requirements,
    packages=find_packages(),
)
