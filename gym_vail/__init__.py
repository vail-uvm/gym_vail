from gym.envs.registration import register

from . import envs


def register_envs():
    register(
        id="Bandit-v0",
        entry_point="gym_vail.envs:Bandit",
        max_episode_steps=100,
        reward_threshold=85.0,
    )

    register(
        id="Maze-v0",
        entry_point="gym_vail.envs:Maze",
        max_episode_steps=250,
        reward_threshold=120.0,
    )
    register(
        id="Maze-v1",
        entry_point="gym_vail.envs:VisualMaze",
        max_episode_steps=250,
        reward_threshold=120.0,
    )

    register(
        id="Memory-v0",
        entry_point="gym_vail.envs:MemoryGame",
        max_episode_steps=250,
        reward_threshold=50.0,
    )
    register(
        id="Memory-v1",
        entry_point="gym_vail.envs:MemoryGame",
        max_episode_steps=250,
        reward_threshold=50.0,
        kwargs={"card_number": 100},
    )
    register(
        id="Memory-v2",
        entry_point="gym_vail.envs:MemoryGame",
        max_episode_steps=250,
        reward_threshold=50.0,
        kwargs={"card_number": 1000},
    )
    register(
        id="Memory-v3",
        entry_point="gym_vail.envs:MemoryGame",
        max_episode_steps=250,
        reward_threshold=50.0,
        kwargs={"card_number": 52},
    )
    register(
        id="Memory-v4",
        entry_point="gym_vail.envs:MNISTMemory",
        max_episode_steps=250,
        reward_threshold=50.0,
    )
    register(
        id="Memory-v5",
        entry_point="gym_vail.envs:MNISTMemory",
        max_episode_steps=250,
        reward_threshold=50.0,
        kwargs={"card_number": 20},
    )

    register(
        id="TabularMDP-v0",
        entry_point="gym_vail.envs:TabularMDP",
        max_episode_steps=500,
        reward_threshold=935.0,
    )
