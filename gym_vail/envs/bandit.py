import gym
import numpy as np
from gym import spaces


class Bandit(gym.Env):
    """
    A multi-armed bandit problem constructed to replicate the environment discussed in:
        https://arxiv.org/abs/1611.02779
    """

    def __init__(self, arm_number=50, seed=None, max_steps=100):
        if seed is not None:
            np.random.seed(seed)

        self.arm_number = arm_number
        self.arms = np.random.uniform(size=self.arm_number)
        self.observation_space = spaces.MultiDiscrete([1])
        self.action_space = spaces.Discrete(arm_number)
        self.last_reward = None
        self.last_action = None

        # used to determine when the game is done
        self.step_count = 0
        self.max_steps = max_steps

    def step(self, action: int):
        self.last_reward = np.random.binomial(1, self.arms[action])
        self.last_action = action

        self.step_count += 1
        done = True if self.step_count >= self.max_steps else False
        return np.zeros((1,), dtype=int), self.last_reward, done, {}

    def reset(self, reset_step_count=True):
        self.arms = np.random.uniform(size=self.arm_number)
        self.step_count = 0
        return np.zeros((1,), dtype=int)

    def render(self, mode="human"):
        if mode == "human":
            print(
                f"Step: {self.step_count}, Action: {self.last_action}, Reward: {self.last_reward}"
            )
        else:
            return {
                "step": self.step_count,
                "action": self.last_action,
                "reward": self.last_reward,
            }


if __name__ == "__main__":
    env = Bandit()
    env.reset()
    # Increase the number of iterations in order to actually see the render
    for _ in range(500):
        action = env.action_space.sample()
        obs, reward, done, info = env.step(action)
        # Comment out or remove when profiling
        print(obs, action, reward)
