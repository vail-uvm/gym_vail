from .bandit import Bandit
from .maze import Maze, VisualMaze
from .memory_game import MemoryGame, MNISTMemory
from .tabular_mdp import TabularMDP

all_envs = [Bandit, Maze, MemoryGame, TabularMDP]
