import gym
import numpy as np


class TabularMDP(gym.Env):
    """
    A tabular markov decision process constructed to replicate the environment
    discussed in:
        https://arxiv.org/abs/1611.02779

    In a single episode the agent is exposed to the same tabular MDP `n_trials`
    times, where each trial has `n_steps_per_trial` steps, thus each episode is
    composed of n_trials * n_steps_per_trial

    I have reversed the usage of episode and trial relative to the RL^2 paper,
    since this implementation signals the end of an episode when the MDP
    configuration changes (end of a trial in the RL^2 paper).
    """

    def __init__(
        self, n_actions=5, n_states=10, n_steps_per_trial=10, n_trials=50, seed=None,
    ):
        if seed is not None:
            np.random.seed(seed)

        self.n_states = n_states
        self.n_actions = n_actions
        self.observation_space = gym.spaces.MultiBinary(self.n_states + 1)
        self.action_space = gym.spaces.Discrete(self.n_actions)

        self.one_hot_states = np.eye(self.n_states, dtype=np.int8)
        self.transition_table = np.zeros((self.n_states, self.n_actions, self.n_states))
        self.reward_means = np.random.normal(1, 1, self.n_states)

        self.last_action = None
        self.current_state = 0
        self.last_reward = None

        # used to determine when the game is done
        self.step_count = 0
        self.n_trials = n_trials
        self.n_steps_per_trial = n_steps_per_trial
        self.max_steps = n_trials * n_steps_per_trial

    def step(self, action: int):
        self.last_action = action
        if self.check_trial_done():
            self.current_state = 0
        else:
            self.current_state = np.random.choice(
                self.n_states, p=self.transition_table[self.current_state, action]
            )
        self.last_reward = np.random.normal(self.reward_means[self.current_state], 1)
        self.step_count += 1

        trial_done = self.check_trial_done()
        episode_done = True if self.step_count >= self.max_steps else False
        return (
            np.concatenate([self.one_hot_states[self.current_state], [trial_done]]),
            self.last_reward,
            episode_done,
            {},
        )

    def check_trial_done(self):
        return self.step_count % self.n_steps_per_trial == 0

    def reset(self):
        self.transition_table[:] = self.make_transition_table()
        self.reward_means = np.random.normal(1, 1, self.n_states)
        self.step_count = 0
        self.current_state = 0
        return np.concatenate([self.one_hot_states[self.current_state], [False]])

    def render(self, mode="human"):
        if mode == "human":
            print(
                f"Step {self.step_count}:"
                f"\n\tTrial Done: {self.check_trial_done()}"
                f"\n\tState:      {self.current_state}"
                f"\n\tAction:     {self.last_action}"
                f"\n\tReward:     {self.last_reward}"
            )
        else:
            return {
                "step": self.step_count,
                "state": self.current_state,
                "action": self.last_action,
                "reward": self.last_reward,
            }

    def make_transition_table(self):
        tt = np.random.dirichlet(
            np.ones(self.n_states), size=(self.n_states, self.n_actions)
        )
        return tt / tt.sum(axis=-1, keepdims=True)


if __name__ == "__main__":
    env = TabularMDP()
    env.reset()
    for _ in range(500):
        action = env.action_space.sample()
        obs, reward, done, info = env.step(action)
        # Comment out or remove when profiling
        env.render(mode="human")
