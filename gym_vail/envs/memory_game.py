import gym
import numpy as np
import tensorflow as tf
from gym import spaces
from matplotlib import cm, colors
from PIL import Image

# Allows the use of MemoryGame on machines without displays (servers)
try:
    from gym.envs.classic_control.rendering import SimpleImageViewer
except Exception as e:
    import logging

    logging.warning(
        "Failed to import SimpleImageViewer from OpenAI Gym. "
        f"Rendering will not function for gym_vail.MemoryGame!:"
        f"\nError details: {e}"
    )


class MemoryGame(gym.Env):
    """
    Implements an OpenAI Gym compatible environment modeled after the game
    memory/concentration

    Observation:
        Type: MultiDiscrete(card_number)
            Full board of cards with shown values
            0         -> Unshown card
            [1 ... n] -> Shown card

    Actions:
        Type: Discrete(card_number)
            Flip the card at the corresponding position in the array

    Reward:
        Find a match     -> 1
        Otherwise        -> 0

    Starting State:
        Board is randomly shuffled and all cards are hidden.

    Episode Termination:
        250 flips have been executed
    """

    def __init__(self, card_number=10, seed=None, max_steps=250):
        # card number must be even to have proper matches
        if card_number % 2 != 0:
            raise ValueError(
                f"Card number should be an even number, given {card_number}."
            )

        if seed is not None:
            np.random.seed(seed)

        self.observation_space = spaces.MultiDiscrete(
            [card_number + 1 for _ in range(card_number)]
        )
        self.action_space = spaces.Discrete(card_number)
        self.card_number = card_number
        self.cards, self.shown = MemoryGame.make_cards(card_number)
        self.complete = np.zeros_like(self.cards, dtype=np.uint8)

        # stores card that should be matched if one is shown, None otherwise
        self.recent_actions = np.zeros(2, dtype=int) - 1
        self.recent_flips = np.zeros(2, dtype=int) - 1
        self.recent_index = 0

        # used to determine when the game is done
        self.step_count = 0
        self.max_steps = max_steps

        self.viewer = None

    @staticmethod
    def make_cards(card_number: int, group_size: int = 2):
        # Make a numpy array of cards
        cards = np.arange(card_number // 2)

        # Duplicate cards to make pairs and reserve 0 for unshown cards
        cards = np.tile(cards, group_size) + 1

        # Randomize the board
        np.random.shuffle(cards)
        return cards, np.zeros(len(cards), dtype=bool)

    def get_view(self):
        return self.cards * self.shown

    def step(self, action: int):
        # Clean up board state at the beginning of a step so that the agent can
        # see the results of its actions before any resetting occurs.
        # Reset the board and provide a reward if all cards are matched
        if self.shown.sum() == len(self.shown):
            self.reset(reset_step_count=False)

        # Rewind recent actions following invalid flips
        if self.invalid_flips():
            for ind in self.recent_actions:
                if not self.complete[ind]:
                    self.shown[ind] = False
            self.recent_actions[:] = -1
            self.recent_flips[:] = -1
            self.recent_index = 0

        if not self.complete[action]:
            self.shown[action] = True
            self.recent_actions[self.recent_index] = action
            self.recent_flips[self.recent_index] = self.cards[action]
            self.recent_index = (self.recent_index + 1) % len(self.recent_actions)

        if self.valid_flips():
            reward = 1.0
            self.complete[self.recent_actions] = 1
            self.recent_actions[:] = -1
            self.recent_flips[:] = -1
            self.recent_index = 0
        else:
            reward = 0.0

        self.step_count += 1
        done = True if self.step_count >= self.max_steps else False
        return self.get_view(), reward, done, {}

    def valid_flips(self):
        return (
            np.all(self.recent_flips >= 0)
            and not np.all(
                self.recent_actions == self.recent_actions[self.recent_index]
            )
            and self.complete[self.recent_actions].sum() == 0
            and np.all(self.recent_flips == self.recent_flips[self.recent_index])
        )

    def invalid_flips(self):
        return np.all(self.recent_flips >= 0) and np.any(
            self.recent_flips != self.recent_flips[self.recent_index]
        )

    def get_image(self, cmap="viridis"):
        im = np.expand_dims(self.get_view(), axis=0)

        # Convert to RGB
        im = cm.ScalarMappable(
            cmap=cmap, norm=colors.Normalize(vmin=0, vmax=self.cards.max()),
        ).to_rgba(im)[..., :3]

        # Cast to 8 bit pixels simplify downstream usage
        im = (255 * im).astype(np.uint8)
        return im

    def render(self, mode="human"):
        img = self.get_image()
        if mode == "human":
            if self.viewer is None:
                self.viewer = SimpleImageViewer()
            self.viewer.imshow(
                np.asarray(Image.fromarray(img).resize((512, 64)), dtype=np.uint8)
            )
            return self.viewer.isopen
        else:
            return img

    def reset(self, reset_step_count=True):
        np.random.shuffle(self.cards)
        self.shown[:] = False
        self.complete[:] = 0
        if reset_step_count:
            self.step_count = 0
        return self.get_view()


class MNISTMemory(MemoryGame):
    @staticmethod
    def get_mnist():
        (train_x, train_y), (test_x, test_y) = tf.keras.datasets.mnist.load_data()
        inputs = np.concatenate([train_x, test_x])
        labels = np.concatenate([train_y, test_y])
        sort_inds = np.argsort(labels, axis=0)
        inputs_sorted = np.take_along_axis(
            inputs, sort_inds[..., np.newaxis, np.newaxis], axis=0
        )
        labels_sorted = np.take_along_axis(labels, sort_inds, axis=0)
        _, split_inds = np.unique(labels_sorted, return_index=True)
        return np.split(inputs_sorted, split_inds[1:])

    def __init__(self, card_number=10, seed=None, max_steps=250):
        if card_number // 2 > 10:
            raise ValueError(
                f"{self.__class__} only has support for digits 0-9. "
                f"Request for {card_number} cards with {card_number // 2} digits can't be fulfilled."
            )

        super().__init__(card_number=card_number, seed=seed, max_steps=max_steps)
        self.observation_space = spaces.Box(0, 1, shape=(28, 28 * card_number, 1))
        self.images = self.get_mnist()
        self.masked = np.zeros((28, 28), dtype=np.uint8)
        self.image_inds = [np.random.randint(len(self.images[x])) for x in self.cards]

    def get_view(self):
        digits = []
        for digit, shown, ind in zip(self.cards, self.shown, self.image_inds):
            if shown:
                digits.append(self.images[digit][ind])
            else:
                digits.append(self.masked)

        return np.tile(np.concatenate(digits, axis=1)[..., np.newaxis], (1, 1, 3))

    def get_image(self, cmap="viridis"):
        return self.get_view()

    def reset(self, reset_step_count=True):
        super().reset(reset_step_count=reset_step_count)
        self.image_inds = [np.random.randint(len(self.images[x])) for x in self.cards]


if __name__ == "__main__":
    import time

    visual = True

    if not visual:
        env = MemoryGame()
    else:
        env = MNISTMemory()

    env.reset()
    env.render(mode="human")
    for _ in range(500):
        action = env.action_space.sample()
        obs, reward, done, info = env.step(action)
        env.render(mode="human")
        time.sleep(0.05)
