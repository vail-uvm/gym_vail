import gym
import numpy as np
from gym import spaces
from matplotlib import cm
from PIL import Image

# Allows the use of Maze on machines without displays (servers)
try:
    from gym.envs.classic_control.rendering import SimpleImageViewer
except Exception as e:
    import logging

    logging.warning(
        "Failed to import SimpleImageViewer from OpenAI Gym. "
        f"Rendering will not function for gym_vail.Maze!:"
        f"\nError details: {e}"
    )


class Maze(gym.Env):
    """
    Implements an OpenAI Gym compatible environment modeled after the maze task
    described in https://arxiv.org/abs/1804.02464.

    The authors provide a reference implementation:
        https://github.com/uber-research/differentiable-plasticity/blob/master/maze/maze.py

    Deviations from the reference:
        - Agent starts from a random initial position, rather than the center.

    Note: Rendering is extremely expensive relative to other operations.
    Rolling out the maze for 5000 steps with a random policy takes ~20 seconds
    with rendering, but only 87 milliseconds without.

    Observation:
        Type: MultiDiscrete(9)
            Agent field of view, represented as a discrete vector.
            0 -> Traversable space
            1 -> Non-traversable space

    Actions:
        Type: Discrete(4)
            0 -> Move downward
            1 -> Move upward
            2 -> Move right
            3 -> Move left

    Reward:
        Collision with wall          -> -0.1
        Reach hidden reward location -> 10

    Starting State:
        Random location in the maze.

    Episode Termination:
        Episode length is greater than 250.
    """

    metadata = {"render.modes": ["human"]}

    ACTION_LOOKUP = {
        0: np.array([-1, 0], dtype=int),
        1: np.array([1, 0], dtype=int),
        2: np.array([0, -1], dtype=int),
        3: np.array([0, 1], dtype=int),
    }

    def __init__(self, maze_type="grid", maze_width=9, view_width=3, max_steps=250):
        if not (maze_width % 2):
            raise ValueError(
                f"Side length should be an odd number, given {maze_width}."
            )

        self.observation_space = spaces.MultiDiscrete(
            [2 for _ in range(view_width * view_width)]
        )
        self.action_space = spaces.Discrete(4)

        self.view_width = view_width
        self.view_radius = self.view_width // 2
        self.border_width = 1
        self.maze_width = maze_width + 2 * self.border_width
        self.maze_type = maze_type
        self.center = self.maze_width // 2

        self.maze = np.empty((self.maze_width, self.maze_width), dtype=int)
        getattr(self, f"{maze_type}_maze")()

        self.agent_view = np.zeros(self.view_width ** 2, dtype=int)

        self.agent_pos = np.empty(2, dtype=int)
        self.reward_pos = np.empty(2, dtype=int)
        self.target_pos = np.empty(2, dtype=int)

        self.place_agent()
        self.place_reward()

        self.max_steps = max_steps
        self.step_count = 0

        self.viewer = None

    def cross_maze(self):
        self.maze[:, :] = 1
        self.maze[self.center, 1:-1] = 0
        self.maze[1:-1, self.center] = 0

    def double_t_maze(self):
        self.maze[:, :] = 1
        self.maze[self.center, 1:-1] = 0
        self.maze[1:-1, 1] = 0
        self.maze[1:-1, -2] = 0

    def grid_maze(self):
        self.maze[:, :] = 1
        self.maze[1:-1, 1:-1] = 0
        self.maze[2:-2:2, 2:-2:2] = 1
        self.maze[self.center, self.center] = 0

    def place_agent(self):
        ind = np.random.choice(*np.nonzero(self.maze.flatten() == 0))
        self.agent_pos[:] = (ind // self.maze_width, ind % self.maze_width)
        self.target_pos[:] = self.agent_pos
        self.update_view()

    def place_reward(self):
        ind = np.random.choice(*np.nonzero(self.maze.flatten() == 0))
        self.reward_pos[:] = (ind // self.maze_width, ind % self.maze_width)

    def update_view(self):
        self.agent_view[:] = self.maze[
            self.agent_pos[0]
            - self.view_radius : self.agent_pos[0]
            + self.view_radius
            + 1,
            self.agent_pos[1]
            - self.view_radius : self.agent_pos[1]
            + self.view_radius
            + 1,
        ].flatten()

    def step(self, action: int):
        try:
            self.target_pos += self.ACTION_LOOKUP[action]
        except KeyError:
            raise ValueError(f"Invalid action, given {action}, expected 0, 1, 2, or 3.")

        reward = 0.0
        if self.maze[tuple(self.target_pos)]:
            reward -= 0.1
            self.target_pos[:] = self.agent_pos
        else:
            self.agent_pos[:] = self.target_pos
            self.update_view()

        if np.all(self.agent_pos == self.reward_pos):
            reward += 10
            self.place_agent()

        self.step_count += 1
        done = True if self.step_count >= self.max_steps else False
        return self.agent_view.copy(), reward, done, {}

    def reset(self):
        self.place_agent()
        self.place_reward()
        self.step_count = 0
        return self.agent_view.copy()

    def get_image(self, cmap="viridis", show_reward=False):
        im = self.maze.copy()

        # Paint the agent and reward into the env
        if show_reward:
            im[tuple(self.reward_pos)] = 2
        im[tuple(self.agent_pos)] = 3

        # Convert to RGB
        im = cm.ScalarMappable(cmap=cmap).to_rgba(im)[..., :3]

        # Cast to 8 bit pixels simplify downstream usage
        im = (255 * im).astype(np.uint8)
        return im

    def render(self, mode="human"):
        img = self.get_image(show_reward=True)
        if mode == "human":
            if self.viewer is None:
                self.viewer = SimpleImageViewer()
            self.viewer.imshow(
                np.asarray(Image.fromarray(img).resize((512, 512)), dtype=np.uint8)
            )
            return self.viewer.isopen
        else:
            return img

    def close(self):
        if self.viewer:
            self.viewer.close()
            self.viewer = None


class VisualMaze(Maze):
    def __init__(self, maze_type="grid", maze_width=9, max_steps=250):
        if not (maze_width % 2):
            raise ValueError(
                f"Side length should be an odd number, given {maze_width}."
            )

        self.border_width = 1
        self.maze_width = maze_width + 2 * self.border_width
        self.maze_type = maze_type
        self.center = self.maze_width // 2

        self.observation_space = spaces.Box(
            0, 1, shape=(self.maze_width, self.maze_width, 3)
        )
        self.action_space = spaces.Discrete(4)

        self.maze = np.empty((self.maze_width, self.maze_width), dtype=int)
        getattr(self, f"{maze_type}_maze")()

        self.agent_view = np.zeros(self.observation_space.shape)

        self.agent_pos = np.empty(2, dtype=int)
        self.reward_pos = np.empty(2, dtype=int)
        self.target_pos = np.empty(2, dtype=int)

        self.place_agent()
        self.place_reward()

        self.max_steps = max_steps
        self.step_count = 0

        self.viewer = None

    def update_view(self):
        self.agent_view[:] = self.get_image(show_reward=False)


if __name__ == "__main__":
    import time

    visual = False

    if visual:
        env = VisualMaze()
    else:
        env = Maze()

    env.reset()
    env.render(mode="human")
    for _ in range(500):
        env.step(env.action_space.sample())
        env.render(mode="human")
        time.sleep(0.05)
    env.close()
